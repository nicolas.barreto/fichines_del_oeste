import "pixi.js";
import "pixi-spine";
import * as createjs from "createjs-module";
import { Stage } from "./Stage";

export enum Direction {
    LEFT = -1,
    RIGHT = 1
};

export class Hero extends PIXI.Container {
    private jumping:boolean = false;

    life:number = 100;
    speedX:number = 10;
    speedY:number = 50;

    constructor(){
        super();

        this.name = "Rick";

        let character:PIXI.spine.Spine = new PIXI.spine.Spine(PIXI.loaders.shared.resources.spineboy.spineData);
        character.state.setAnimation(0, 'running', true);
        character.height = Stage.height * .3;
        character.scale.x = character.scale.y;

        this.addChild(character);
    }

    move(direction:Direction):void {
        this.x += this.speedX * direction;
        this.scale.x = Math.abs(this.scale.x) * direction;

        if(this.x + (this.width * .5) < 0) this.x = -this.width * .5;
        if(this.x + (this.width * .5) > Stage.width) this.x = Stage.width - (this.width * .5);
    }

    jump(): void{
        if(!this.jumping){
            this.jumping = true;

            let startY:number = this.y;
            let tweenObj:any = {value: 0};

            createjs.Tween.get(tweenObj)
                .to({value: Math.PI}, 250)
                .call( () => {
                    this.jumping = false 
                })
                .on("change", () => {
                    this.y = startY - Math.round(Math.sin(tweenObj.value) * this.speedY);
                });
        }
    }
}