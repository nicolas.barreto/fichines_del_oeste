import "pixi.js";
import "pixi-spine";
import {Direction, Hero} from './Hero';
import {Enemy} from './Enemy';
import {Bullet} from './Bullet';
import {Background} from './background/Background';
import { MouseInteraction } from "../utils/MouseInteraction";
import { KeyboardInteraction, KeyboardKey } from "../utils/KeyboardInteraction";
import {clone}from 'lodash';


const NUM_ENEMIES:number = 4;
const ENEMY_MAP:Array<{name:string, x:number, y:number}> = [
    {name: "Blinky", x: 200, y: 200},
    {name: "Inky", x: 500, y: 200},
    {name: "Pinky", x: 200, y: 500},
    {name: "Clyde", x: 500, y: 500}
];

export class Stage {
    static width:number;
    static height:number;

    private background:Background;
    private hero:Hero;
    private enemies:Array<Enemy> = new Array<Enemy>();
    private bullets:Array<Bullet> = new Array<Bullet>();

    private keyFlags:any = {
        [KeyboardKey.UP_ARROW]: false,
        [KeyboardKey.DOWN_ARROW]: false,
        [KeyboardKey.LEFT_ARROW]: false,
        [KeyboardKey.RIGHT_ARROW]: false,
    };

    //Tiempo para el calculo del tiempo en el superMegaLoop
    private lastUpdateTime:number;

    constructor(private stage:PIXI.Container, private width:number, private height: number){
        Stage.width = this.width;
        Stage.height = this.height;

        this.createLevel();
        
        //Agrego el loop principal
        PIXI.ticker.shared.add(() => {
            this.superMegaLoop()
        });
       
    }

    // Obviamente el super mega loop
    private superMegaLoop(){
        this.calculePositions();
        this.calculateCollisions();
        this.moveHero();
    }

    private calculePositions(): void {
        // Calculo el tiempo que paso desde el ultimo tick
        let previousUpdateTime = this.lastUpdateTime;
        this.lastUpdateTime = (window.performance && performance.now()) || Date.now();
        let cTime         = this.lastUpdateTime - previousUpdateTime;
	    let duration      = 1000; // todas las velocidades estan expresadas en pixels/1000 miliseconds
	     
        
        //hubico al heroe en su nuevo lugar

        //Ubico los bullets en su nueva posicion
        for (let i = 0; i < this.bullets.length; i++) {
            let bullet = this.bullets[i];
            
            // Calculo cuanto tiene que haber avanzado cada boton
            let _displacementX = bullet.speedX*cTime/duration; 
            let _displacementY = bullet.speedY*cTime/duration; 

            bullet.x += _displacementX;
            bullet.y += _displacementY;
            if ( bullet.y < 0 || bullet.y > this.background.height){
                this.stage.removeChild(bullet);
                this.bullets[i] = null;  
            }
        }

        // remuevo las bullets que estan fuera de la pantalla
        this.bullets = this.bullets.filter(e => e != null)

        function callculePosition(){

        }
    }

    private calculateCollisions(): void {
        for (let i = 0; i < this.enemies.length; i++) {
            let enemy:Enemy = this.enemies[i];
            let hitArea:PIXI.Rectangle = <PIXI.Rectangle>clone(enemy.hitArea);
            hitArea.x += enemy.x;
            hitArea.y += enemy.y;
            for (let j = 0; j < this.bullets.length; j++) {
                let bullet:Bullet = this.bullets[j];
                //console.log(enemy.hitArea);
                //console.log(bullet.x +" /" + bullet.y);
                
                if(hitArea.contains(bullet.x, bullet.y)){
                    
                    enemy.loselives(1);
                    if (enemy.life <= 0){
                        enemy.die();
                    }
                }

            }
            
        }
    }

    private moveHero(): void {
        if(this.keyFlags[KeyboardKey.LEFT_ARROW]){
            this.hero.move(Direction.LEFT);
        }
        if(this.keyFlags[KeyboardKey.RIGHT_ARROW]){
            this.hero.move(Direction.RIGHT);
        }
        if(this.keyFlags[KeyboardKey.UP_ARROW]){
            this.hero.jump();
            this.keyFlags[KeyboardKey.UP_ARROW] = false;
        }
    }

    private createLevel(): void {
        MouseInteraction.onClick.add(this.mouseHandler, this);
        // Register keys
        KeyboardInteraction.onKeyUp_UP.add(this.keyboardUpHandler.bind(this, KeyboardKey.UP_ARROW));
        KeyboardInteraction.onKeyUp_DOWN.add(this.keyboardUpHandler.bind(this, KeyboardKey.DOWN_ARROW));
        KeyboardInteraction.onKeyUp_LEFT.add(this.keyboardUpHandler.bind(this, KeyboardKey.LEFT_ARROW));
        KeyboardInteraction.onKeyUp_RIGHT.add(this.keyboardUpHandler.bind(this, KeyboardKey.RIGHT_ARROW));

        KeyboardInteraction.onKeyDown_UP.add(this.keyboardDownHandler.bind(this, KeyboardKey.UP_ARROW));
        KeyboardInteraction.onKeyDown_DOWN.add(this.keyboardDownHandler.bind(this, KeyboardKey.DOWN_ARROW));
        KeyboardInteraction.onKeyDown_LEFT.add(this.keyboardDownHandler.bind(this, KeyboardKey.LEFT_ARROW));
        KeyboardInteraction.onKeyDown_RIGHT.add(this.keyboardDownHandler.bind(this, KeyboardKey.RIGHT_ARROW));

        // Add background
        this.background = new Background();
        this.stage.addChild(this.background);
        
        // Add hero
        this.hero = new Hero();
        this.hero.x = Stage.width * .5;
        this.hero.y = Stage.height * .85;
        this.stage.addChild(this.hero);
        
        // Add enemies
        for(let i = 0; i < NUM_ENEMIES; i++){
            let enemyPos:any = ENEMY_MAP[i];
            let enemy:Enemy = new Enemy(enemyPos.name);
            
            enemy.x = enemyPos.x;
            enemy.y = enemyPos.y;

            this.enemies.push(enemy);
            this.stage.addChild(enemy);

            enemy.appear();
        }
    }

    private mouseHandler(e:PIXI.interaction.InteractionEvent): void{
        for (let i = 0; i < this.enemies.length; i++) {
            let enemy = this.enemies[i];
                
            if(enemy.hitArea.contains(e.data.global.x, e.data.global.y)){
                console.log("Le pegué a " + enemy.name);
            }          
        }

        this.shoot(new PIXI.Point(e.data.global.x, e.data.global.y));
    }

    private shoot(target:PIXI.Point): void {
        let bullet = new Bullet(<PIXI.Point>this.hero.position, target);
        this.stage.addChild( bullet );
        this.bullets.push( bullet );
    }

    private keyboardUpHandler(keyCode:KeyboardKey): void {
        this.keyFlags[keyCode] = false;
    }

    private keyboardDownHandler(keyCode:KeyboardKey): void {
        this.keyFlags[keyCode] = true;
    }
}