import * as PIXI from 'pixi.js';
import { Stage } from './Stage';
import { MouseInteraction } from './../utils/MouseInteraction';
import { KeyboardInteraction } from '../utils/KeyboardInteraction';

type Dimensions = {
    width:number,
    height:number
};

export default class Game {
    private dimensions: Dimensions = {
        width: 1024,
        height: 768
    };
    private app: PIXI.Application;
    private stage: Stage;

    constructor() {
        this.app = new PIXI.Application({
            width: this.dimensions.width,
            height: this.dimensions.height,
            antialias: true,
            transparent: false,
            resolution: 1
	});

	document.body.appendChild(this.app.view);
    }

    init(): void {
        this.app.ticker.add(() => {
            //console.log("rendering");
        });
        
        new KeyboardInteraction();
        new MouseInteraction(this.app.stage);

        let loader:PIXI.loaders.Loader= PIXI.loaders.shared;
        loader.add("background", "assets/images/background.jpg");
        loader.add("spineboy", "assets/images/pixie.json");
        loader.load(() => {
            this.stage = new Stage(this.app.stage, this.dimensions.width, this.dimensions.height);
        });
    }
}