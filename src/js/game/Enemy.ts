import "pixi.js";
import "pixi-spine";
import * as createjs from "createjs-module";
import { Stage } from "./Stage";

export class Enemy extends PIXI.Container {
    life = 1;
    
    constructor(name?:string){
        super();

        this.name = name;

        let character:PIXI.spine.Spine = new PIXI.spine.Spine(PIXI.loaders.shared.resources.spineboy.spineData);
        character.state.setAnimation(0, 'running', true);
        character.height = Stage.height * .3;
        character.scale.x = character.scale.y;

        this.addChild(character);

        this.hitArea = this.getBounds();
    }

    appear(): void {
        this.alpha = 0;

        createjs.Tween.get(this)
            .to({alpha: 1}, 1000);
    }

    loselives(livesLost:number){
        this.life -= livesLost;
    }

    die(): void {
        createjs.Tween.get(this)
            .to({alpha: 0}, 1000);
    }
}