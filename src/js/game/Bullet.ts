import "pixi.js";
import "pixi-spine";

export class Bullet extends PIXI.Container {
    private baseSpeed = 400;
    speedX:number = 0; //pixels per second ponele
    speedY:number = 0; //pixels per second ponele
    
    constructor(heroPosition:PIXI.Point, targetPosition:PIXI.Point){
        super();
        /*let character:PIXI.spine.Spine = new PIXI.spine.Spine(PIXI.loaders.shared.resources.spineboy.spineData);
        character.state.setAnimation(0, 'running', true);*/
        //this.origin = new PIXI.Point(heroPosition.x, heroPosition.y);
        //this.target = new PIXI.Point(targetPosition.x, targetPosition.y);
        let alphaX= targetPosition.x - heroPosition.x;
        let alphaY= targetPosition.y - heroPosition.y;
        let hipo = Math.sqrt(alphaX*alphaX + alphaY* alphaY );
        let factor = this.baseSpeed / hipo;

        this.speedX = alphaX * factor;
        this.speedY = alphaY * factor;

        let size = 4;
        let bullet:PIXI.Graphics = new PIXI.Graphics();
        bullet.beginFill(0xFF0000);
        bullet.drawCircle(0, 0, 5);
        bullet.endFill();

        this.x=heroPosition.x;
        this.y=heroPosition.y;
        this.addChild(bullet);
    }
}