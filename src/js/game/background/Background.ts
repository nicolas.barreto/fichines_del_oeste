import "pixi.js";
import { Stage } from "../Stage";

export class Background extends PIXI.Container {
    constructor() {
        super();

        this.name = "background";

        this.addBackground();
    }
    
    private addBackground(): void {
        let background:PIXI.Sprite = PIXI.Sprite.from("background");

        background.width = Stage.width;
        background.height = Stage.height;

        this.addChild(background);
    }

}