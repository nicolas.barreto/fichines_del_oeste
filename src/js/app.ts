declare var require: any;
require('../css/main.css');

import Game from './game/Game';

class App {
	private _game: Game;

	constructor(game: Game) {
		this._game = game;
	}

	public setup(): void {
		// Any setup that is required that only runs once before game loads goes here

        this._game.init();
	}
}

window.onload = () => {
	(<any>window).createjs = (<any>window).createjs || {};
	
	let app = new App(new Game());

	app.setup();
}