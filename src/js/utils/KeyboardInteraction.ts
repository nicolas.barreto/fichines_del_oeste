import { Dictionary } from 'typescript-collections';
import { Signal } from 'signals';

export class KeyboardListener {
    constructor(public callback: (e:any) => any, public params: any[], public scope: any) {
    }
}

export class KeyboardInteraction {
    static onKeyUp_UP:Signal;
    static onKeyUp_DOWN:Signal;
    static onKeyUp_LEFT:Signal;
    static onKeyUp_RIGHT:Signal;

    static onKeyDown_UP:Signal;
    static onKeyDown_DOWN:Signal;
    static onKeyDown_LEFT:Signal;
    static onKeyDown_RIGHT:Signal;

    private static KEY_DOWN_EVENT: string = "keydown";
    private static KEY_UP_EVENT: string = "keyup";

    private keyboardUpEvents: Dictionary<KeyboardKey, KeyboardListener[]> = new Dictionary<KeyboardKey, KeyboardListener[]>();
    private keyboardDownEvents: Dictionary<KeyboardKey, KeyboardListener[]> = new Dictionary<KeyboardKey, KeyboardListener[]>();

    private handleKeyboardDownEvents: any;
    private handleKeyboardUpEvents: any;

    private whiteListedKeys: boolean[]

    private _enabled: boolean = true;

    constructor() {
        this.handleKeyboardDownEvents = this.handleEvent.bind(this, this.keyboardDownEvents);
        this.handleKeyboardUpEvents = this.handleEvent.bind(this, this.keyboardUpEvents);

        this.whiteListedKeys = [];

        KeyboardInteraction.onKeyUp_UP = new Signal();
        KeyboardInteraction.onKeyUp_DOWN = new Signal();
        KeyboardInteraction.onKeyUp_LEFT = new Signal();
        KeyboardInteraction.onKeyUp_RIGHT = new Signal();
 
        KeyboardInteraction.onKeyDown_UP = new Signal();
        KeyboardInteraction.onKeyDown_DOWN = new Signal();
        KeyboardInteraction.onKeyDown_LEFT = new Signal();
        KeyboardInteraction.onKeyDown_RIGHT = new Signal();

        this.addListeners();

        this.addKeyUp(KeyboardKey.UP_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyUp_UP], this);
        this.addKeyUp(KeyboardKey.DOWN_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyUp_DOWN], this);
        this.addKeyUp(KeyboardKey.LEFT_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyUp_LEFT], this);
        this.addKeyUp(KeyboardKey.RIGHT_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyUp_RIGHT], this);
        
        this.addKeyDown(KeyboardKey.UP_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyDown_UP], this);
        this.addKeyDown(KeyboardKey.DOWN_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyDown_DOWN], this);
        this.addKeyDown(KeyboardKey.LEFT_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyDown_LEFT], this);
        this.addKeyDown(KeyboardKey.RIGHT_ARROW, this.dispatchSignal, [KeyboardInteraction.onKeyDown_RIGHT], this);
        
        this.addWhitelistedKey(KeyboardKey.UP_ARROW);
        this.addWhitelistedKey(KeyboardKey.DOWN_ARROW);
        this.addWhitelistedKey(KeyboardKey.LEFT_ARROW);
        this.addWhitelistedKey(KeyboardKey.RIGHT_ARROW);
    }

    private dispatchSignal(signal:Signal) {
        signal.dispatch();
    }

    private addListeners() {
        $(window).on(KeyboardInteraction.KEY_DOWN_EVENT, this.handleKeyboardDownEvents);
        $(window).on(KeyboardInteraction.KEY_UP_EVENT, this.handleKeyboardUpEvents);
    }

    private removeListeners() {
        $(window).off(KeyboardInteraction.KEY_DOWN_EVENT, this.handleKeyboardDownEvents);
        $(window).off(KeyboardInteraction.KEY_UP_EVENT, this.handleKeyboardUpEvents);
    }

    private handleEvent(events: Dictionary<KeyboardKey, KeyboardListener[]>, event: JQueryKeyEventObject): void {
        var key: KeyboardKey = event.keyCode;
        if (this._enabled || (this.whiteListedKeys[key] != null && this.whiteListedKeys[key])) {
            var listeners: KeyboardListener[] = events.getValue(key);
            if (listeners != null) {
                for (var listener of listeners) {
                    listener.callback.apply(listener.scope, listener.params);
                }
            }
        }
    }

    //TODO REMOVE LISTENERS
    set enabled(value: boolean) {
        if (this._enabled != value) {
            this._enabled = value;
        }
    }


    //key down
    addKeyDown(key: KeyboardKey, callback: (e:any) => any, params: any[], scope: any): void {
        var listeners: KeyboardListener[] = this.getListeners(this.keyboardDownEvents, key);
        listeners.push(new KeyboardListener(callback, params, scope));
    }


    removeKeyDown(key: KeyboardKey, callback: (e:any) => any, params: any[], scope: any): void {
        var listeners: KeyboardListener[] = this.getListeners(this.keyboardDownEvents, key);
        for (var listener of listeners) {
            if (listener.callback === callback && listener.scope == scope) {
                //remove
                listeners.splice(listeners.indexOf(listener), 1);
            }
        }
    }

    //key up
    addKeyUp(key: KeyboardKey, callback: (e:any) => any, params: any[], scope: any): void {
        var listeners: KeyboardListener[] = this.getListeners(this.keyboardUpEvents, key);
        listeners.push(new KeyboardListener(callback, params, scope));
    }

    removeKeyUp(key: KeyboardKey, callback: (e:any) => any, params: any[], scope: any): void {
        var listeners: KeyboardListener[] = this.getListeners(this.keyboardUpEvents, key);
        for (var listener of listeners) {
            if (listener.callback === callback && listener.scope == scope) {
                //remove
                listeners.splice(listeners.indexOf(listener), 1);
            }
        }
    }

    private getListeners(dictionary: Dictionary<KeyboardKey, KeyboardListener[]>, key: KeyboardKey): KeyboardListener[] {
        var listeners: KeyboardListener[];
        if (dictionary.containsKey(key)) {
            listeners = dictionary.getValue(key);
        } else {
            listeners = [];
            dictionary.setValue(key, listeners);
        }
        return listeners;
    }

    addWhitelistedKey(key: KeyboardKey) {
        this.whiteListedKeys[key] = true;
    }

    removeWhitelistedKey(key: KeyboardKey) {
        this.whiteListedKeys[key] = false;
    }
}

export enum KeyboardKey {
    BACKSPACE = 8,
    TAB = 9,
    ENTER = 13,
    SHIFT = 16,
    CTRL = 17,
    ALT = 18,
    PAUSE_BREAK = 19,
    CAPS_LOCK = 20,
    ESCAPE = 27,
    SPACE = 32,
    PAGE_UP = 33,
    PAGE_DOWN = 34,
    END = 35,
    HOME = 36,
    LEFT_ARROW = 37,
    UP_ARROW = 38,
    RIGHT_ARROW = 39,
    DOWN_ARROW = 40,
    INSERT = 45,
    DELETE = 46,
    NUM_0 = 48,
    NUM_1 = 49,
    NUM_2 = 50,
    NUM_3 = 51,
    NUM_4 = 52,
    NUM_5 = 53,
    NUM_6 = 54,
    NUM_7 = 55,
    NUM_8 = 56,
    NUM_9 = 57,
    A = 65,
    B = 66,
    C = 67,
    D = 68,
    E = 69,
    F = 70,
    G = 71,
    H = 72,
    I = 73,
    J = 74,
    K = 75,
    L = 76,
    M = 77,
    N = 78,
    O = 79,
    P = 80,
    Q = 81,
    R = 82,
    S = 83,
    T = 84,
    U = 85,
    V = 86,
    W = 87,
    X = 88,
    Y = 89,
    Z = 90,
    LEFT_WINDOW_KEY = 91,
    RIGHT_WINDOW_KEY = 92,
    SELECT_KEY = 93,
    NUMPAD_0 = 96,
    NUMPAD_1 = 97,
    NUMPAD_2 = 98,
    NUMPAD_3 = 99,
    NUMPAD_4 = 100,
    NUMPAD_5 = 101,
    NUMPAD_6 = 102,
    NUMPAD_7 = 103,
    NUMPAD_8 = 104,
    NUMPAD_9 = 105,
    MULTIPLY = 106,
    ADD = 107,
    SUBTRACT = 109,
    DECIMAL_POINT = 110,
    DIVIDE = 111,
    F1 = 112,
    F2 = 113,
    F3 = 114,
    F4 = 115,
    F5 = 116,
    F6 = 117,
    F7 = 118,
    F8 = 119,
    F9 = 120,
    F10 = 121,
    F11 = 122,
    F12 = 123,
    NUM_LOCK = 144,
    SCROLL_LOCK = 145,
    SEMI_COLON = 186,
    EQUAL_SIGN = 187,
    COMMA = 188,
    DASH = 189,
    PERIOD = 190,
    FORWARD_SLASH = 191,
    GRAVE_ACCENT = 192,
    OPEN_BRACKET = 219,
    BACK_SLASH = 220,
    CLOSE_BRAKET = 221,
    SINGLE_QUOTE = 222
}
