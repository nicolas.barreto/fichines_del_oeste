import { Signal } from 'signals';

export class MouseInteraction {
    static onClick:Signal;

    constructor(private resource:PIXI.Container) {
        MouseInteraction.onClick = new Signal();

        this.addListener();
    }   
    
    protected addListener():void {
        this.resource.interactive = true;

        //click
        this.resource.on("click", this.handleClick, this);
        this.resource.on("tap", this.handleClick, this);

        /*
        //down
        this.resource.on("mousedown", this.handleMouseDown, this);
        this.resource.on("touchstart", this.handleMouseDown, this);

        //up
        this.resource.on("touchend", this.handleMouseUp, this);
        this.resource.on("touchendoutside", this.handleMouseUp, this);
        this.resource.on("mouseup", this.handleMouseUp, this);
        this.resource.on("mouseupoutside", this.handleMouseUp, this);

        //over
        this.resource.on("mouseover", this.handleOver, this);

        //out
        this.resource.on("mouseout", this.handleOut, this);
        */
    }

    private handleClick(e:MouseEvent): void {
        MouseInteraction.onClick.dispatch(e);
    }
/*
    protected removeListener():void {
        this.resource.interactive = false;

        //click
        this.resource.off("click", this.handleClick, this);
        this.resource.off("tap", this.handleClick, this);

        //down
        this.resource.off("mousedown", this.handleMouseDown, this);
        this.resource.off("touchstart", this.handleMouseDown, this);

        //up
        this.resource.off("touchend", this.handleMouseUp, this);
        this.resource.off("touchendoutside", this.handleMouseUp, this);
        this.resource.off("mouseup", this.handleMouseUp, this);
        this.resource.off("mouseupoutside", this.handleMouseUp, this);

        //over
        this.resource.off("mouseover", this.handleOver, this);

        //out
        this.resource.off("mouseout", this.handleOut, this);
    } 
*/
}