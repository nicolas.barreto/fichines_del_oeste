import { Dictionary } from 'typescript-collections';

export class ClassParameters {
    constructor(public classType: any, public parameters: Array<any>) {
    }
}

//TODO Refactor name to Singleton Mapper, review all
export class Injector {
    private static CLASS_MAP_PREFIX: string = "class.";


    private classById: Dictionary<string, ClassParameters> = new Dictionary<string, ClassParameters>();
    private instances: Dictionary<string, ClassParameters> = new Dictionary<string, ClassParameters>();

    constructor() {
    }


    mapInstance(id: string, instance: any): any {
        this.instances.setValue(id, instance);
        return instance;
    }

    mapClass(id: string, classType: any, params: Array<any> = null): void {
        this.classById.setValue(id, new ClassParameters(classType, params || []));
    }

    mapInstanceByClass(classType: any, instance: any): any {
        this.instances.setValue(this.generateClassId(classType), instance);
        return instance;
    }

    injectByClass(classType: any, params: Array<any> = null): any {
        return this.inject(this.generateClassId(classType), params);
    }

    tryInjectByClass(classType: any, params: Array<any> = null): any {
        return this.hasInstanceMappedByClass(classType) ? this.inject(this.generateClassId(classType), params) : null;
    }

    clearClassInstance(classType: any): void {
        var id: string = this.generateClassId(classType);
        if (this.isClassMapped(id)) {
            this.instances.remove(id);
        }
    }

    clearInstance(id: string): void {
        if (this.isInstanceMapped(id)) {
            this.instances.remove(id);
        }
    }

    isClassMapped(classType: any): boolean {
        var id: string = this.generateClassId(classType);
        return this.instances.containsKey(id);
    }

    isInstanceMapped(instanceId: string): boolean {
        return this.instances.containsKey(instanceId);
    }


    private hasInstanceMappedByClass(classType: any): boolean {
        return this.instances.containsKey(this.generateClassId(classType));
    }

    inject(id: string, params: Array<any> = null): any {

        if (!this.instances.containsKey(id)) {
            if (!this.classById.containsKey(id)) {
                throw new Error("Class id: " + id + " is not mapped.")
            }
            var instance: any = this.createInstance(id, params);
            this.mapInstance(id, instance);
        }
        return this.instances.getValue(id);
    }

    private createInstance(id: string, params: Array<any> = null): void {
        var classDescriptor: ClassParameters = this.classById.getValue(id);
        var parameters: Array<any> = params == null ? classDescriptor.parameters : classDescriptor.parameters.concat(params);
        return new classDescriptor.classType.apply(parameters);
    }

    private generateClassId(classType: any): string {
        return Injector.CLASS_MAP_PREFIX + this.getClassName(<any>classType);
    }

    private getClassName(classType: any): string {
        //now the way to generate an unique hash of class
        return window.btoa(classType.toLocaleString());
    }
}


export var injector: Injector = new Injector();